#include "keyboard.h"
#include "../cpu/types.h"
#include "screen.h"
#include "../cpu/isr.h"
#include "../libc/string.h"
#include "ports.h"
#include "../kernel/kernel.h"

static char key_buffer[KEY_BUFFER_LENGTH];

const char *sc_name[] = { "ERROR", "Esc", "1", "2", "3", "4", "5", "6", 
	"7", "8", "9", "0", "-", "=", "Backspace", "Tab", "Q", "W", "E", 
	"R", "T", "Y", "U", "I", "O", "P", "[", "]", "Enter", "Lctrl", 
	"A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "`", 
	"LShift", "\\", "Z", "X", "C", "V", "B", "N", "M", ",", ".", 
	"/", "RShift", "Keypad *", "LAlt", "Spacebar"};
const char sc_ascii[] = { '?', '?', '1', '2', '3', '4', '5', '6',     
	'7', '8', '9', '0', '-', '=', '?', '?', 'q', 'w', 'e', 'r', 't', 'y', 
	'u', 'i', 'o', 'p', '[', ']', '?', '?', 'a', 's', 'd', 'f', 'g', 
	'h', 'j', 'k', 'l', ';', '\'', '`', '?', '\\', 'z', 'x', 'c', 'v', 
	'b', 'n', 'm', ',', '.', '/', '?', '?', '?', ' '};

static void keyboard_callback(regs_t regs) {
	u8 scancode = port_byte_in(KEYBOARD_DATA_PORT_IN);
	
	if (scancode > SC_MAX)
		return;
	if (scancode == ENTER) {
		kprint("\n");

		user_input(key_buffer);
		key_buffer[0] = '\0';
	}
	else if (scancode == BACKSPACE) {

		if (key_buffer[0] == '\0')
			return;

		kprint_backspace();

		size_t length = strlen(key_buffer);
		key_buffer[length - 1] = '\0';
	}
	else {
		size_t len = strlen(key_buffer);

		if (len >= KEY_BUFFER_LENGTH - 1) {
			return;
		}
		char str[2] = {sc_ascii[scancode], '\0'};
		kprint(str);

		key_buffer[len] = str[0];
		key_buffer[len + 1] = '\0';
	}
}

void
init_keyboard()
{
	register_interrupt_handler(IRQ1, keyboard_callback);
}
