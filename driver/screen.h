#ifndef SCREEN_H
#define SCREEN_H

#include "../cpu/types.h"

#define VGA_CTRL_REGISTER 0x3d4
#define VGA_DATA_REGISTER 0x3d5
#define VGA_OFFSET_LOW 0x0f
#define VGA_OFFSET_HIGH 0x0e
#define VIDEO_ADDRESS 0xb8000
#define MAX_ROWS 25
#define MAX_COLS 80
#define WHITE_ON_BLACK 0x0f

void set_cursor(u64 offset);
u64 get_cursor();
void kprint_at(char* string, u8 at_cursor, u64 row, u64 col);
void kprint(char *string);
void kprint_backspace();
void clear_screen();

#endif
