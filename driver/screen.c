#include "screen.h"
#include "ports.h"
#include "../libc/mem.h"
#include "../cpu/types.h"

void set_char_at_video_memory(char character, int offset);
u64 scroll_ln(u64 offset);
u64 get_offset(u64 col, u64 row);
u64 get_row_from_offset(u64 offset);
u64 move_offset_to_new_line(u64 offset);

void
set_cursor(u64 offset)
{
	offset /= 2;
	port_byte_out(VGA_CTRL_REGISTER, VGA_OFFSET_HIGH);
	port_byte_out(VGA_DATA_REGISTER, (unsigned char) (offset >> 8));
	port_byte_out(VGA_CTRL_REGISTER, VGA_OFFSET_LOW);
	port_byte_out(VGA_DATA_REGISTER, (unsigned char) (offset & 0xff));
}

u64
get_cursor()
{
	port_byte_out(VGA_CTRL_REGISTER, VGA_OFFSET_HIGH);
	u64 offset = port_byte_in(VGA_DATA_REGISTER) << 8;
	port_byte_out(VGA_CTRL_REGISTER, VGA_OFFSET_LOW);
	offset += port_byte_in(VGA_DATA_REGISTER);
	return offset * 2;
}

void
set_char_at_video_memory(char character, int offset)
{
    unsigned char *vidmem = (unsigned char *) VIDEO_ADDRESS;
    vidmem[offset] = character;
    vidmem[offset + 1] = WHITE_ON_BLACK;
}

void
kprint_at(char *string, u8 at_cursor, u64 row, u64 col)
{
	u64 offset;
	size_t i = 0;

	if (at_cursor > 0)
		offset = get_cursor();
	else {
		offset = get_offset(row, col);
	}

	while (string[i] != 0) {
		if (offset >= MAX_ROWS * MAX_COLS * 2) {
			offset = scroll_ln(offset);
		}
		if (string[i] == '\n') {
			offset = move_offset_to_new_line(offset);
		} else {
			set_char_at_video_memory(string[i], offset);
			offset += 2;
		}
		i++;
	}
	set_cursor(offset);
}

void
kprint(char *string)
{
	kprint_at(string, 1, 0, 0);
}

void
kprint_backspace()
{
	u64 offset = get_cursor() - 2;
	set_char_at_video_memory(' ', offset);
	set_cursor(offset);
}

u64
scroll_ln(u64 offset)
{
    memcpy(
            (u8 *) (get_offset(1, 0) + VIDEO_ADDRESS),
            (u8 *) (get_offset(0, 0) + VIDEO_ADDRESS),
            MAX_COLS * (MAX_ROWS - 1) * 2
    );

    for (int col = 0; col < MAX_COLS; col++) {
        set_char_at_video_memory(' ', get_offset(MAX_ROWS - 1, col));
    }

    return offset - 2 * MAX_COLS;
}

void
clear_screen()
{
    for (int i = 0; i < MAX_COLS * MAX_ROWS; ++i) {
        set_char_at_video_memory(' ', i * 2);
    }
    set_cursor(get_offset(0, 0));
}

u64
get_offset(u64 row, u64 col)
{
    return 2 * (row * MAX_COLS + col);
}

u64
get_row_from_offset(u64 offset)
{
    return offset / (2 * MAX_COLS);
}

u64
move_offset_to_new_line(u64 offset)
{
    return get_offset(get_row_from_offset(offset) + 1, 0);
}