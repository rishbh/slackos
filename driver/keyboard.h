#ifndef KEYBOARD_H
#define KEYBOARD_H
#define BACKSPACE 0x0E
#define ENTER 0x1C
#define SC_MAX 57

#define KEY_BUFFER_LENGTH 80
#define KEYBOARD_DATA_PORT_IN 0x60

void init_keyboard();

#endif
