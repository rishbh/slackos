#include "ports.h"
/*in : read port data
out: write port data

function to read port data */
u8
port_byte_in(u16 port)
{
	/*mapping C variable port into the dx register
	execute in al,dx
	store value of al register into C variable result */
	unsigned char result;
	__asm__("in %%dx, %%al" : "=a" (result) : "d" (port));
	return result;
}

/*function to write port data*/
void
port_byte_out(u16 port, u8 data)
{
	/*it executes out dx,al*/
	__asm__("out %%al, %%dx" : : "a" (data), "d" (port));
}