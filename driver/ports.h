#ifndef PORTS_H
#define PORTS_H

#include "../cpu/types.h"
u8 port_byte_in(u16 port);
void port_byte_out(u16 port, u8 data);
void set_char_at_video_memory(char character, int offset);

#endif
