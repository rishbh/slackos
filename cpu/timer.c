#include "timer.h"
#include "../cpu/isr.h"
#include "../driver/screen.h"
#include "../libc/string.h"
#include "../driver/ports.h"

u64 tick = 0;

static void
timer_callback(regs_t regs) {
	tick++;
}

void
init_timer(u64 freq) {
	register_interrupt_handler(IRQ0, timer_callback);

	u64 divisor = PIT_MAX_FREQ / freq;
	u8 low = divisor & 0xFF;
	u8 high = (divisor >> 8) & 0xFF;

	port_byte_out(PIT_COMMAND_PORT, PIT_MODE);
	port_byte_out(PIT_DATA_PORT, low);
	port_byte_out(PIT_DATA_PORT, high);
}
