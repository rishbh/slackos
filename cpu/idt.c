#include "idt.h"
#include "../libc/string.h"

idt_gate_t idt[IDT_ENTRIES];
idt_register_t idt_reg;

void
set_idt_gate(u8 n, u64 handler)
{
	idt[n].low_offset = (u16)(handler & 0xFFFF);
	idt[n].sel = KERNEL_CS;
	idt[n].ist = 0;
	idt[n].flags = 0x8E;
	idt[n].mid_offset = (u16)((handler >> 16) & 0xFFFF);
	idt[n].high_offset = (u32)((handler >> 32) & 0xFFFFFFFF);
	idt[n].zero = 0;
}

void
set_idt()
{
	idt_reg.base = (u64)&idt;
	idt_reg.limit = IDT_ENTRIES * sizeof(idt_gate_t) - 1;

	__asm__ __volatile__("lidt (%0)" : : "r" (&idt_reg));
}
