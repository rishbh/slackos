#ifndef IDT_H
#define IDT_H

#include "types.h"

#define KERNEL_CS 0x08
#define IDT_ENTRIES 256

typedef struct {
	u16 low_offset;
	u16 sel;
	u8 ist;
	u8 flags;
	u16 mid_offset;
	u32 high_offset;
	u32 zero;
} __attribute__((packed)) idt_gate_t;

typedef struct {
	u16 limit;
	u64 base;
} __attribute__((packed)) idt_register_t;

void set_idt_gate(u8 n, u64 handler);
void set_idt();

#endif
