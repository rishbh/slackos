#ifndef TIMER_H
#define TIMER_H

#include "types.h"

#define PIT_MAX_FREQ 1193180
#define PIT_COMMAND_PORT 0x43
#define PIT_DATA_PORT 0x40
#define PIT_MODE 0x36

void init_timer(u64 freq);

#endif
