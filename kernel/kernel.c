#include "../cpu/isr.h"
#include "../cpu/idt.h"
#include "../driver/keyboard.h"
#include "../cpu/timer.h"
#include "../driver/screen.h"
#include "../libc/string.h"
#include "../libc/stdint.h"

void
user_input(char* string)
{
	char *tok = strtok(&string);
	if (strcmp(tok, "echo") == 0) {
		kprint(string);
		kprint("\n");

	} else if (strcmp(tok, "add") == 0) {
		int num = 0, a, e;
		char ch[12];

		while (*(tok = strtok(&string)) != '\0') {
			e = string_to_int(tok, &a ,MAX_INT_DIGITS + 1);
			switch (e) {
				case E_InsufficientBuffer:
					kprint("insufficient buffer\n");
					return;

				case E_NotNumber:
					kprint("not number\n");
					return;

				case E_Overflow:
					kprint("overflow\n");
					return;
			}
			if (MAX_INT - num < a) {
				kprint("overflow\n");
				return;
			}

	        num += a;
		}
		
		int_to_ascii(num, ch);
		
		kprint(ch);
		kprint("\n");

	} else if (strcmp(tok, "mul") == 0) {

		int num = 1, a, e;
		char ch[12];

		int tmp;
		while (*(tok = strtok(&string)) != '\0') {
			e = string_to_int(tok, &a, MAX_INT_DIGITS + 1);
			switch (e) {
				case E_InsufficientBuffer:
					kprint("insufficient buffer\n");
					return;

				case E_NotNumber:
					kprint("not number\n");
					return;

				case E_Overflow:
					kprint("overflow\n");
					return;
			}
			
	        tmp = num;
	        num *= a;
	        
	        if (num / tmp != a) {
				kprint("overflow\n");
				return;
			}
		}
		
		int_to_ascii(num, ch);
		
		kprint(ch);
		kprint("\n");

	} else if (strcmp(tok, "div") == 0) {
		int num, a, b, e;
		char ch[12];

		
		e = string_to_int(strtok(&string), &a, MAX_INT_DIGITS + 1);
		switch (e) {
			case E_InsufficientBuffer:
				kprint("insufficient buffer\n");
				return;

			case E_NotNumber:
				kprint("not number\n");
				return;
				
			case E_Overflow:
				kprint("overflow\n");
				return;
			}
			
		e = string_to_int(strtok(&string), &b, MAX_INT_DIGITS + 1);
		switch (e) {
			case E_InsufficientBuffer:
				kprint("insufficient buffer\n");
				return;

			case E_NotNumber:
				kprint("not number\n");
				return;

			case E_Overflow:
				kprint("overflow\n");
				return;

		}

		if (string[0] != '\0') {
			kprint("more than two operands\n");
			return;
		}

		num = a / b;
		int_to_ascii(num, ch);
		kprint(ch);
		kprint("\n");
			
	} else if (strcmp(tok, "sub") == 0) {

		int num, a, b, e;
		char ch[12];

		
		e = string_to_int(strtok(&string), &a, MAX_INT_DIGITS + 1);
		switch (e) {
			case E_InsufficientBuffer:
				kprint("insufficient buffer\n");
				return;

			case E_NotNumber:
				kprint("not number\n");
				return;

			case E_Overflow:
				kprint("overflow\n");
				return;

			}
			
		e = string_to_int(strtok(&string), &b, MAX_INT_DIGITS + 1);
		switch (e) {
			case E_InsufficientBuffer:
				kprint("insufficient buffer\n");
				return;

			case E_NotNumber:
				kprint("not number\n");
				return;

			case E_Overflow:
				kprint("overflow\n");
				return;


		/*TODO: add underflow and overflow detection*/
		}

		if (string[0] != '\0') {
			kprint("more than two operands\n");
			return;
		}
		
		num = a - b;
		int_to_ascii(num, ch);
		
		kprint(ch);
		kprint("\n");

	} else if (strcmp(tok, "halt") == 0) {
		kprint("bye bye !!!\n");
		__asm__ __volatile__("hlt");
	} else {
		kprint("command not found\n");
	}
}

void 
kmain()
{
	isr_install();
	irq_install();
}
