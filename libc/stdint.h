#ifndef STDINT_H
#define STDINT_H

#define MAX_INT 2147483647
#define MIN_INT -2147483648
#define MAX_INT_DIGITS 10

#endif
