#ifndef STRING_H
#define STRING_H

#include "../cpu/types.h"

enum Error {
        E_InsufficientBuffer = 1,
        E_Overflow = 2,
        E_NotNumber = 3
};

void int_to_ascii(int n, char str[]);
int string_to_int(char str[], int *num, size_t bflen);
void reverse(char str[]);
size_t strlen(char str[]);
int strcmp(char *str1, char* str2);
char* strtok(char **rest);
int is_digit(char c);

#endif
