#include "string.h"
#include "stdint.h"

void
int_to_ascii(int n, char str[])
{
	int i, sign;

	if ((sign = n) < 0) n = -n;

	i = 0;
	do {
		str[i++] = n % 10 + '0';
	} while ((n /= 10) > 0);

	if (sign < 0) str[i++] = '-';
	str[i] = '\0';

	reverse(str);
}

int
string_to_int(char str[], int *num, size_t bflen)
{
	int j = 1;
	*num = 0;
	int prev;
	size_t len = strlen(str);

	if (len >= bflen)
		return E_InsufficientBuffer;

	for (size_t i = len; i > 0; i--) {
		if (!is_digit(str[i - 1]))
			return E_NotNumber;

		if (len - i == 9) {
			if (str[i - 1] > '2')
				return E_Overflow;
			else if (str[i - 1] == '2' && *num > 147483648)
				return E_Overflow;
		}

		prev = *num;

		*num += (str[i - 1] - '0') * j;
		j *= 10;
	}
	return 0;
}

void
reverse(char str[]) {
	char tmp;

	for (size_t i = 0, j = strlen(str) - 1; i < j; i++, j--) {
		tmp = str[i];
		str[i] = str[j];
		str[j] = tmp;
	}
}

size_t
strlen(char str[]) {
	size_t i = 0;

	for (i = 0; str[i] != '\0'; i++);
	return i;
}

int
strcmp(char *str1, char* str2)
{
	int i;
	for (i = 0; str1[i] == str2[i]; i++) {
		if (str1[i] == '\0')
			return 0;
	}
	return (int)str1[i] - (int)str2[i];
}

char *
strtok(char **rest)
{
	char *tok = *rest;
	size_t i = 0;
	while (1) {
		if (tok[i] == ' ') {
			tok[i] = '\0';
			while (tok[i + 1] == ' ') i++;
			*rest = &tok[i + 1];
			break;
		} else if (tok[i] == '\0') {
			*rest = &tok[i];
			break;
		}
		i++;
	}
	return tok;
}

int
is_digit(char d)
{
	return (d >= '0' && d <= '9');
}
