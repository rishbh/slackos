#include "mem.h"

void
memcpy(u8 *source, u8 *dest, size_t nbytes)
{
	int i;

	for (i = 0; i < nbytes; i++) {
		*(dest + i) = *(source + i);
	}
}

void
memset(void *dest, u8 val, size_t len)
{
	u8 *ptr;

	for (ptr = (u8 *)dest; len != 0; ptr++, len--)
		*ptr = val;
}
