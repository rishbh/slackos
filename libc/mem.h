#ifndef MEM_H
#define MEM_H

#include "../cpu/types.h"

void memcpy(u8 *source, u8 *dest, size_t nbytes);
void memset(void *dest, u8 val, size_t len);

#endif
